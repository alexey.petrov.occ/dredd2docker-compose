FROM apiaryio/dredd

RUN mkdir -p /app
WORKDIR /app

COPY api-description.yml .
CMD ["dredd"]
