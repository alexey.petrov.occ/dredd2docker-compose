# dredd2docker-compose

*[Dredd](https://dredd.org/en/latest/) is a language-agnostic command-line tool for validating API description document against backend implementation of the API.* To orginize this validation it is proposed to use [Docker](https://docs.docker.com/get-started/). Docker is the most suitable way (for now days) to build, test and destribute your software. [docker-compose](https://docs.docker.com/compose/) utility provides a standard way to automate this validation process.

**Note** that *dredd* specific *docker-compose* related tips & tricks see [here](https://dredd.org/en/latest/installation.html#docker-compose)

**Note** that the way it is integrated with native GitLab CI/CD were inspired by ["Run docker-compose build in .gitlab-ci.yml"](https://stackoverflow.com/a/42697808) discussion.
