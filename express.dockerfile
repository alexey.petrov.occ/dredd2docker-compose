FROM node:alpine

RUN mkdir -p /app
WORKDIR /app

COPY package-lock.json .
COPY package.json .
RUN npm install

EXPOSE 3000
COPY index.js .
CMD ["node", "index.js"]
